<?php

namespace Models;

use Core\Model;


/**
 * Class UserModel
 * @package Models
 */
class UserModel extends Model
{
    /**
     * @var array
     */
    protected $data = [];

    /**
     * UserModel constructor.
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * @param string $email
     * @return bool
     */
    public function isEmailUnique(string $email)
    {
        $rows = $this->db->select('*', 'users', "email='{$email}'");

        return empty($rows);
    }

    /**
     * @param string $name
     * @param string $email
     * @param string $pass
     * @return mixed
     */
    public function saveUser(string $name, string $email, string $pass)
    {
        $data = [
            'id' => null,
            'name' => $name,
            'email' => $email,
            'password' => $pass
        ];

        return $this->db->insert(
            'users (id, name, email, password)',
            '(:id, :name, :email, :password)',
            $data
        );
    }

    /**
     * @param string $email
     * @return mixed
     */
    public function getPassword(string $email)
    {
        return $this->db->select('password', 'users', "email='{$email}'")[0]['password'];
    }

    /**
     * @param int|null $id
     * @return array|mixed
     */
    public function loadDataById(int $id = null)
    {
        if (!$id && !$id = $this->getActiveUserId()) {
            return [];
        }

        $this->data = $this->db->select('*', 'users', "id='{$id}'")[0];

        return $this->data;
    }

    /**
     * @param string $email
     * @return array|mixed
     */
    public function loadDataByEmail(string $email)
    {
        $this->data = $this->db->select('*', 'users', "email='{$email}'")[0];

        return $this->data;
    }

    /**
     * Returns an authorized user ID if it exists
     *
     * @return mixed|null
     */
    public function getActiveUserId()
    {
        if (isset($_SESSION['user_id']) && $_SESSION['user_id']) {
            return $_SESSION['user_id'];
        }

        return null;
    }

    /**
     * @param int|null $userId
     * @return array
     */
    public function getUserAttributes(int $userId = null)
    {
        if (!$userId && !$userId = $this->getActiveUserId()) {
            return [];
        }

        $query = "
            SELECT attributes.id, attributes.code, attributes.type, user_attribute_value.value
            FROM user_attribute_value
                LEFT JOIN attributes ON user_attribute_value.attr_id = attributes.id
            WHERE user_id = {$userId}
        ";

        $rows = $this->db->queryFetched($query);
        $attributes = [];
        foreach ($rows as $row) {
            $attributes[$row['code']] = $row;
        }

        return $attributes;
    }

    /**
     * Returns a list of existing user attributes
     * @return array
     */
    public function getAttributesList()
    {
        $rows = $this->db->select('*', 'attributes');
        $list = [];
        foreach ($rows as $row) {
            $list[$row['code']] = $row;
        }

        return $list;
    }

    /**
     * @param $attributes
     * @return false|mixed
     */
    public function updateUserAttributes($attributes)
    {
        if (!$attributes) {
            return false;
        }

        $userId = $this->getActiveUserId();
        $query = '';

        foreach ($attributes as $attr) {
            $query .= "
                INSERT INTO user_attribute_value (user_id, attr_id, value)
                VALUES ('{$userId}', '{$attr['id']}', '{$attr['value']}')
                ON DUPLICATE KEY UPDATE value = '{$attr['value']}';\n
            ";
        }

        return $this->db->query($query);
    }

    /**
     * @param $data
     * @return false|mixed
     */
    public function updateUser($data)
    {
        if (!$data) {
            return false;
        }

        $userId = $this->getActiveUserId();
        $query = '';

        foreach ($data as $code => $val) {
            $query .= "
                UPDATE users
                SET {$code} = '{$val}'
                WHERE id = {$userId};\n
            ";
        }

        return $this->db->query($query);
    }
}
