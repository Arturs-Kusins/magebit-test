<?php

namespace Core;


/**
 * Class Controller
 * @package Core
 */
abstract class Controller {
    /**
     * @var string
     */
    protected $contentView = 'default';

    /**
     * @var string
     */
    protected $layoutView = 'default';

    /**
     * @var View
     */
    private $view;

    /**
     * @var
     */
    protected $data;

    /**
     * Controller constructor.
     */
    function __construct()
    {
        $this->view = new View();
        $this->data['title'] = 'MageBit Test Task';
    }

    /**
     * Default action
     */
    abstract function actionIndex();

    /**
     * Default render method
     */
    protected function renderView() {
        $this->view->render($this->contentView, $this->layoutView, $this->data);
    }
}
