<?php

namespace Core;

use Controllers\ErrorsController;


/**
 * Class ExceptionHandler
 * @package Core
 */
class ExceptionHandler
{
    /**
     * ExceptionHandler constructor.
     */
    public function __construct()
    {
        @set_exception_handler(array($this, 'exception_handler'));
    }

    /**
     * Default exception handler
     * @param $exception
     */
    public function exception_handler($exception)
    {
        error_log($exception->getMessage());
        (new ErrorsController())->action500();
    }
}
