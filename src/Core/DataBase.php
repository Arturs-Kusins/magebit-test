<?php

namespace Core;


/**
 * Class DataBase
 * @package Core
 */
class DataBase
{
    /**
     * @var null
     */
    private static $instance;

    /**
     * @var
     */
    private $pdo;

    /**
     * DataBase constructor.
     */
    private function __construct() {
        $this->createConnection();
    }

    /**
     * @return DataBase
     */
    public static function getInstance()
    {
        if (static::$instance === null) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    /**
     * @param string $select
     * @param string $from
     * @param string|null $where
     * @return mixed
     */
    public function select(string $select, string $from, string $where = null) {
        if ($where) {
            $sth = $this->pdo->query("SELECT {$select} FROM {$from} WHERE {$where}");
        } else {
            $sth = $this->pdo->query("SELECT {$select} FROM {$from}");
        }
        $sth->setFetchMode(\PDO::FETCH_ASSOC);

        return $sth->fetchAll();
    }

    /**
     * @param string $into
     * @param string $values
     * @param array $data
     * @return mixed
     */
    public function insert(string $into, string $values, array $data)
    {
        $sth = $this->pdo->prepare("INSERT INTO {$into} VALUES {$values}");

        return $sth->execute($data);
    }

    /**
     * @param string $query
     * @return mixed
     */
    public function queryFetched(string $query)
    {
        $sth = $this->pdo->query($query);
        $sth->setFetchMode(\PDO::FETCH_ASSOC);

        return $sth->fetchAll();
    }

    /**
     * @param string $query
     * @return mixed
     */
    public function query(string $query)
    {
        return $this->pdo->query($query);
    }

    /**
     * Creates connection to a database
     */
    protected function createConnection()
    {
        $cfg = include ROOTPATH . DIRECTORY_SEPARATOR . 'config.php';
        $this->pdo = new \PDO(
            "{$cfg['db_type']}:host={$cfg['db_host']};dbname={$cfg['db_name']}",
            $cfg['db_user'],
            $cfg['db_pass']
        );
    }

    private function __clone() {}

    private function __wakeup() {}
}
