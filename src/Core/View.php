<?php

namespace Core;

/**
 * Class View
 * @package Core
 */
class View
{
    /**
     * @param $contentView
     * @param $layoutView
     * @param null $data
     */
    function render($contentView, $layoutView, $data = null)
    {
        $contentView .= '.phtml';
        $layoutView .= '.phtml';

        if (is_array($data)) {
            extract($data);
        }

        include ROOTPATH . '/src/Layouts/' . $layoutView;
    }
}
