<?php

namespace Core;

/**
 * Class Model
 * @package Core
 */
abstract class Model
{
    /**
     * @var DataBase
     */
    protected $db;

    /**
     * @var array
     */
    protected $data = [];

    /**
     * Model constructor.
     */
    function __construct()
    {
        $this->db = DataBase::getInstance();
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }
}
