<?php

namespace Core;

use Controllers\ErrorsController;


/**
 * Class Router
 * @package Core
 */
class Router
{
    /**
     * @var string
     */
    protected $defaultController = 'Login';

    /**
     * @var string
     */
    protected $defaultAction = 'Index';

    /**
     * @throws \Exception
     */
    public function run()
    {
        $controllerName = $this->defaultController;
        $actionName = $this->defaultAction;
        $routes = explode('/', $_SERVER['REQUEST_URI']);

        if (!empty($routes[1])) {
            $controllerName = ucfirst(strtolower($routes[1]));
        }
        if (!empty($routes[2])) {
            $actionName = ucfirst(strtolower($routes[2]));
        }

        $controllerName = $controllerName . 'Controller';
        $controllerFile = ROOTPATH . '/src/Controllers/' . $controllerName . '.php';
        if (!file_exists($controllerFile)) {
            error_log("Controller {$controllerName} doesn't exist");
            (new ErrorsController())->action404();

            return;
        }
        include $controllerFile;
        $controllerClass = 'Controllers' . DIRECTORY_SEPARATOR . $controllerName;
        $controller = new $controllerClass;

        $actionName = 'action' . $actionName;
        if (!method_exists($controller, $actionName)) {
            error_log("Action {$controllerName}::{$actionName} doesn't exist");
            (new ErrorsController())->action404();

            return;
        }
        $controller->$actionName();
    }
}
