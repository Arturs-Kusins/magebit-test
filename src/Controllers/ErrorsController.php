<?php

namespace Controllers;


use Core\Controller;

class ErrorsController extends Controller
{
    /**
     * @var string
     */
    protected $contentView = '500';

    public function actionIndex()
    {
        $this->renderView();
    }

    public function action404()
    {
        $this->contentView = '404';
        $this->renderView();
    }

    public function action500()
    {
        $this->contentView = '500';
        $this->renderView();
    }
}
