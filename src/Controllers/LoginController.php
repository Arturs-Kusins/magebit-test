<?php

namespace Controllers;

use Core\Controller;
use Models\UserModel;


/**
 * Class LoginController
 * @package Controllers
 */
class LoginController extends Controller
{
    /**
     * @var string
     */
    protected $contentView = 'login';

    /**
     * @var UserModel
     */
    protected $userModel;

    /**
     * LoginController constructor.
     */
    function __construct()
    {
        parent::__construct();

        $this->userModel = new UserModel();
    }

    /**
     * Index action
     */
    public function actionIndex()
    {
        $this->checkAuthorization();
        $this->renderView();
    }

    /**
     * Sign Up action
     */
    public function actionSignup()
    {
        $this->checkAuthorization();
        $this->data['errors'] = [];

        if (!isset($_POST['name'])) {
            $this->data['errors'][] = 'Empty name';
        }

        if (!isset($_POST['email'])) {
            $this->data['errors'][] = 'Empty email';
        }

        if (!isset($_POST['password'])) {
            $this->data['errors'][] = 'Empty password';
        }

        if (!empty($this->data['errors'])) {
            $this->renderView();

            return;
        }

        $userModel = new UserModel();
        $name = filter_var(trim($_POST['name']), FILTER_SANITIZE_STRING);
        $email = filter_var(trim($_POST['email']), FILTER_SANITIZE_EMAIL);
        $pass = $_POST['password'];

        if (!$name) {
            $this->data['errors'][] = 'Incorrect name';
        }

        if (!$email) {
            $this->data['errors'][] = 'Incorrect email';
        }

        if (!$pass) {
            $this->data['errors'][] = 'Incorrect password';
        }

        if (!$userModel->isEmailUnique($email)) {
            $this->data['errors'][] = 'Email address already in use';
        }

        if (!empty($this->data['errors'])) {
            $this->renderView();

            return;
        }

        $pass = password_hash($pass, PASSWORD_DEFAULT);
        $success = $userModel->saveUser($name, $email, $pass);
        if ($success) {
            $data = $userModel->loadDataByEmail($email);
            if (isset($data['id']) && $data['id']) {
                $_SESSION['user_id'] = $data['id'];
                header('Location: /user');
            }
        } else {
            $this->data['errors'][] = 'Something is wrong';
            $this->renderView();
        }
    }

    /**
     * Login action
     */
    public function actionLogin()
    {
        $this->checkAuthorization();
        $this->data['errors'] = [];

        if (!isset($_POST['email'])) {
            $this->data['errors'][] = 'Empty email';
        }

        if (!isset($_POST['password'])) {
            $this->data['errors'][] = 'Empty password';
        }

        if (!empty($this->data['errors'])) {
            $this->renderView();

            return;
        }

        $email = filter_var(trim($_POST['email']), FILTER_SANITIZE_EMAIL);
        $pass = $_POST['password'];

        if (!$email) {
            $this->data['errors'][] = 'Incorrect email';
        }

        if (!$pass) {
            $this->data['errors'][] = 'Incorrect password';
        }

        if (!empty($this->data['errors'])) {
            $this->renderView();

            return;
        }

        $this->userModel->loadDataByEmail($email);
        $userData = $this->userModel->getData();

        if ($userData['password'] && password_verify($pass, $userData['password'])) {
            $_SESSION['user_id'] = $userData['id'];
        } else {
            $this->data['errors'][] = 'Wrong email or password';
            $this->renderView();
        }

        header('Location: /user');
    }

    /**
     * Checks if an user is logged in and redirects to the /user action if so
     */
    protected function checkAuthorization()
    {
        if ($this->userModel->getActiveUserId()) {
            header('Location: /user');
        }
    }
}
