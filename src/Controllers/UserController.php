<?php

namespace Controllers;

use Core\Controller;
use Models\UserModel;


/**
 * Class UserController
 * @package Controllers
 */
class UserController extends Controller
{
    /**
     * @var string
     */
    protected $contentView = 'user';

    /**
     * @var UserModel
     */
    protected $userModel;

    /**
     * UserController constructor.
     */
    function __construct()
    {
        parent::__construct();

        $this->userModel = new UserModel();
    }

    /**
     * Index action
     */
    public function actionIndex()
    {
        $this->checkAuthorization();

        $this->userModel->loadDataById();
        if (isset($this->data['errors'])) {
            $errors = $this->data['errors'];
            $this->data = $this->userModel->getData();
            $this->data['errors'] = $errors;
        } else {
            $this->data = $this->userModel->getData();
        }
        $this->data['attributes'] = $this->userModel->getUserAttributes($this->data['id']);
        $this->data['attributesList'] = $this->userModel->getAttributesList();
        $this->renderView();
    }

    /**
     * Logout action
     */
    public function actionLogout()
    {
        unset($_SESSION['user_id']);
        header('Location: /');
    }

    /**
     * Save action
     */
    public function actionSave() {
        $this->checkAuthorization();

        $toUpdateAttributes = $this->prepareUserAttributes();
        $toUpdateUser = $this->prepareUserData();

        if (empty($this->data['errors'])) {
            $this->userModel->updateUserAttributes($toUpdateAttributes);
            $this->userModel->updateUser($toUpdateUser);
        }

        $this->actionIndex();
    }

    /**
     * Checks if an user is not logged in and redirects to the /login action if so
     */
    protected function checkAuthorization()
    {
        if (!$this->userModel->getActiveUserId()) {
            header('Location: /login');
        }
    }

    /**
     * Sanitizes, check, and prepare user attributes to update
     * @return array
     */
    protected function prepareUserAttributes()
    {
        $sanitizedData = [];
        foreach ($_POST as $k => $v) {
            $sanitizedData[$k] = filter_var($v, FILTER_SANITIZE_STRING);
        }

        $userAttributes = $this->userModel->getUserAttributes();
        $attrList = $this->userModel->getAttributesList();
        $toUpdateAttributes = [];
        foreach ($attrList as $attr) {
            if (
                isset($sanitizedData[$attr['code']]) &&
                $sanitizedData[$attr['code']] != $userAttributes[$attr['code']]['value']
            ) {
                $toUpdateAttributes[] = [
                    'id' => $attr['id'],
                    'value' => $sanitizedData[$attr['code']]
                ];
            }
        }

        return $toUpdateAttributes;
    }

    /**
     * Sanitizes, check, and prepare user data to update
     * @return array
     */
    protected function prepareUserData()
    {
        $userData = $this->userModel->loadDataById();
        $toUpdateUser = [];
        if (isset($_POST['name'])) {
            $name = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
            if ($name) {
                if ($userData['name'] != $name) {
                    $toUpdateUser['name'] = $name;
                }
            } else {
                $this->data['errors'][] = 'Incorrect name';
            }
        }
        if (isset($_POST['email'])) {
            $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
            if ($email) {
                if ($userData['email'] != $email) {
                    if (!$this->userModel->isEmailUnique($email)) {
                        $this->data['errors'][] = 'Email address already in use';
                    } else {
                        $toUpdateUser['email'] = $email;
                    }
                }
            } else {
                $this->data['errors'][] = 'Incorrect email';
            }
        }

        return $toUpdateUser;
    }
}
