let gulp = require('gulp');
let sass = require('gulp-sass');
let browserSync = require('browser-sync').create();


gulp.task('sass', function(){
    return gulp.src('public/scss/**/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('public/css'))
        .pipe(browserSync.stream());
});

gulp.task('watch', function(){
    browserSync.init({
        proxy: "localhost",
    });

    gulp.watch('public/scss/**/*.scss', gulp.series('sass')).on('change', browserSync.reload);
    gulp.watch('public/html/**/*.html', browserSync.reload).on('change', browserSync.reload);
    gulp.watch('public/js/**/*.js', browserSync.reload).on('change', browserSync.reload);
});

gulp.task('default', gulp.series('watch'));
