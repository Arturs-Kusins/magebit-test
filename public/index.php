<?php

use Core\Router;
use Core\ExceptionHandler;

session_start();
define('ROOTPATH', $_SERVER['DOCUMENT_ROOT']);
require_once ROOTPATH . '/vendor/autoload.php';

new ExceptionHandler();
(new Router())->run();
