$(function() {
    let activeFormClass = 'active';
    let signup = $('#signup');
    let login = $('#login');
    let toLogin = $('#to-login');
    let toLoginMob = $('#to-login-mobile');
    let toSignup = $('#to-signup');
    let toSignupMob = $('#to-signup-mobile');
    
    function switchForm() {
        signup.toggleClass(activeFormClass);
        login.toggleClass(activeFormClass);
    }

    $([toLogin, toLoginMob, toSignup, toSignupMob]).each(function() {
        $(this).on('click', function() {
            switchForm();
        });
    });
});
